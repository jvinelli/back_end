var global = require('./globales.js'); //me quedo en global con las variables definidas en globales
var express = require('express'); /*referencia a la variable express o dependencia */
var app = express(); /*creas un objeto de la clase express  Servidor para atender las peticiones node*/
var bodyParser = require('body-parser');
app.use(bodyParser.json()); //acà tomo la variable bodyParser que apunta al body_Parser
var jwt = require('jsonwebtoken');

var port =  process.env.PORT || 3000;
app.listen(port); /*metodo que crea el objeto express y es el puerto por el que va a escuchar*/
console.log('Escuchando puerto 3000 nuevo');

//mlab ************************************************************************
// GET users consumiendo API REST de mLab
app.get(global.uri + 'users',
  function(req, res) {
    console.log("GET users");
    var httpClient = global.requestJSON.createClient(global.baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';                        //string para filtrar el campo _id de mongoDB
    httpClient.get('usermlab?' + queryString + global.apiKeyMLab,       //creamos el Cliente httpClient
      function(err, respuestaMLab, body) {                   //error, respuesta de mLab y el cuerpo
        var response = {}; //variable de tipo objeto vacio
        if(err) { //lo vamos inicializando
            response = { "msg" : "Error obteniendo usuario."  }
            res.status(500); //còdigo de error del servidor
        } else { //si no hay error
          if(body.length > 0) { //body de la function que obtenemos de respuesta y vemos si tiene algo
            response = body;
          } else {
            response = { "msg" : "Usuario no encontrado." }
            res.status(404); //còdigo de error que no se pudo encontrar
          }
        }
        res.send(response); //enviamos la respuesta
      });
});

// Petición GET con id en mLab
app.get(global.uri + 'users/:id', //agregamos :id por ser un usuario puntual
  function (req, res) {
    console.log("GET user/:id");
    console.log(req.params.id);
    var id = req.params.id; //para tomar de los paramotros el id a consultar
    var queryString = 'q={"id":' + id + '}&'; //el tipo q es para filtrar por un campo, entonces filtra por el "id" con el id obtenido antes
    var queryStrField = 'f={"_id":0}&'; //string para filtrar el campo _id de mongoDB
    var httpClient = global.requestJSON.createClient(global.baseMLabURL); //Cliente mLab
    httpClient.get('usermlab?' + queryString + queryStrField + global.apiKeyMLab,
      function(err, respuestaMLab, body){ //error, respuesta de mLab y el cuerpo
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = { "msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = { "msg" : "Usuario no encontrado." }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET Cuentas con id user en mLab
app.get(global.uri + 'cuentas/:id', //agregamos :id por ser un usuario puntual
  function (req, res) {
    console.log("GET cuentas/:id");
    console.log(req.params.id);
    var id = req.params.id; //para tomar de los paramotros el id a consultar
    var queryString = 'q={"idCuenta":' + id + '}&'; //el tipo q es para filtrar por un campo, entonces filtra por el "id" con el id obtenido antes
    var queryStrField = 'f={"_id":0}&'; //string para filtrar el campo _id de mongoDB
    var httpClient = global.requestJSON.createClient(global.baseMLabURL); //Cliente mLab
    console.log('cuentamlab?' + queryString + queryStrField + global.apiKeyMLab);
    httpClient.get('cuentamlab?' + queryString + queryStrField + global.apiKeyMLab,
      function(err, respuestaMLab, body){ //error, respuesta de mLab y el cuerpo
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = { "msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = { "msg" : "Usuario no encontrado." }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// POST 'users' mLab
app.post(global.uri + 'users',
  function(req, res){
    var clienteMlab = global.requestJSON.createClient(global.baseMLabURL); //creamos el Cliente mLab
    clienteMlab.get('usermlab?' + global.apiKeyMLab, //esto es para obtener el nro de registros de la tabla users
      function(error, respuestaMLab, body){ //error, respuesta de mLab y el cuerpo
        newID = body.length + 1; //a la cantidad obtenida antes le sumo 1 para luego hacer el post con un id nuestro
        console.log("newID:" + newID);
        console.log(req.body); //muestro el body para validar
        //var cuentas = [];
        var newUser = { //creamos el objeto js y obtenemos el cuerpo del req
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password,
          "cuentas" : req.body.cuentas
        };
        /*Control existencia usuario por id*/
        var email = req.body.email;
        var existemail = false;
        for(var i=0; i < body.length; i++){
          if (body[i].email == email){
            existemail = true;
          }
        }
        console.log(existemail);
        if (existemail){
            res.status(404).send("email existente");
        } else {
          clienteMlab.post(global.baseMLabURL + "usermlab?" + global.apiKeyMLab, newUser,
          function(error, respuestaMLab, body){
              console.log("Respuesta mLab correcta.");
              var response = {};
              if(error) {
                  response = { "msg" : "Error creando usuario."}
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                  res.status(200);
                } else {
                  response = { "msg" : "Usuario no creado." }
                  res.status(404);
                } // cierra Eles 4o4
              } // cierra Eles 2oo
            res.send(body);
          }); //cierra funcion error y consulta clienteMlab
        }
      });
  });

//PUT of user
app.put(global.uri + 'users/:id',
function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = global.requestJSON.createClient(global.baseMLabURL);
  clienteMlab.get('usermlab?'+ queryStringID + global.apiKeyMLab ,
    function(err, respuestaMLab , body) {
      var response = body[0];
      console.log(response);
      console.log(body);
     var changeUser = { //creamos el objeto js y obtenemos el cuerpo del req
       "id" : req.body.id, //+id,
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "email" : req.body.email,
       "password" : req.body.password,
       "cuentas" : req.body.cuentas
     };
     console.log(changeUser);
     clienteMlab.put('usermlab/' + response._id.$oid + '?' + global.apiKeyMLab, changeUser,
      function(err, respuestaMLab, body) {
        console.log("body:"+ body);
       res.status(200).send(body);
      });
 });
});

//DELETE user with id
app.delete(global.uri + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(global.baseMLabURL + 'usermlab?' + queryStringID + global.apiKeyMLab);
    var httpClient = global.requestJSON.createClient(global.baseMLabURL);
    httpClient.get('usermlab?' +  queryStringID + global.apiKeyMLab,
      function(err, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(global.baseMLabURL + "usermlab/" + respuesta._id.$oid +'?'+ global.apiKeyMLab,
          function(err, respuestaMLab,body){
            res.send(body);
        });
      });
  });




//+++++++++++++++++++++++  LOGIN y LOGOUT  ++++++++++++++++++
//seguridad
//metodo POST Login, cuando hago login voy a hacer un update y un put de un atributo boolean
//falta poner que la clave este cifrada

//Method POST login
app.post(global.uri + "login",
  function (req, res)
  {
    console.log("POST login");
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var queryStringpass = 'q={"password":' + pass + '}&';
    var  clienteMlab = global.requestJSON.createClient(global.baseMLabURL);
    clienteMlab.get('usermlab?'+ queryStringEmail + global.apiKeyMLab ,
    function(err, respuestaMLab , body) {
      console.log("entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta != undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(global.baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + global.apiKeyMLab);
            clienteMlab.put('usermlab?q={"id": ' + respuesta.id + '}&' + global.apiKeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                var tokendata = jwt.sign({data : body[0]}, "Criptografia", {
                                           expiresIn: 300 // expira en 5min
                                       });
                res.send({token:tokendata});
              });
          }
          else {
            res.send({"msg":"contraseña incorrecta"});
          }
      } else {
        console.log("Email Incorrecto");
        res.send({"msg": "email Incorrecto"});
      }
    });
});

//Method POST logout
app.post(global.uri + "logout",
  function (req, res){
    console.log("POST logout");
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = global.requestJSON.createClient(global.baseMLabURL);
    clienteMlab.get('usermlab?'+ queryStringEmail + global.apiKeyMLab ,
    function(err, respuestaMLab, body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta != undefined){
            console.log("logout Correcto");
            var session = {"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + global.apiKeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);  //res.send({"msg": "logout Exitoso"});
              });
      } else {
        console.log("Error en logout");
        res.send({"msg": "Error en logout"});
      }
    });
});
