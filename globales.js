// usuarios.js
const uri = '/api-uruguay/v1/';
const baseMLabURL = 'https://api.mlab.com/api/1/databases/echubduruguay/collections/';
const uriIron = 'http://localhost:3000/api-uruguay/v1/users/';
const apiKeyMLab = 'apiKey=c0QVmDDCQezJaftNB3r50sBGuyrchlax';
const requestJSON = require('request-json');

// export
module.exports.uri = uri;
module.exports.apiKeyMLab = apiKeyMLab;
module.exports.baseMLabURL = baseMLabURL;
module.exports.requestJSON = requestJSON;
