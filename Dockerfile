# Imagen docker base
FROM node:latest

# Definimos directorio de trabajo de docker
WORKDIR /api-uruguay

# copia contenido de nuestro dir del projecto al directorio del WORKDIR
ADD . /api-uruguay

#instalar en caso que no tengamos e node
#RUN dmp install

# puerto donde exponemos el contenedor
EXPOSE 3000

#comando para lanzar la app etiqueta que puse en el package
CMD ["npm", "start"]

# construimos la imagen a partir del sudo docker build -t img-v1 . desde la terminal de linux donde mi imagen es img-v1
