var express = require('express'); /*referencia a la variable express o dependencia */
var app = express(); /*creas un objeto de la clase express  Servidor para atender las peticiones node*/
var port = process.env.PORT || 3000;
const uri = '/api-uruguay/v1/';
var usersfile = require('./users.json');
var cuentasfile = require('./cuentas.json');
// para generar un nuevo usuario necesitamos los datos y la cnatidad del array
//para que el jason se manden los datos, se necesita parsear los datod, como nodemodule lo tiene body_parser usamos escuchar
var bodyParser = require('body-parser'); //acà lo referencio
app.use(bodyParser.json()); //acà tomo la variable bodyParser que apunta al body_Parser



//GET users
app.get(uri + 'users',
    function(req, res) /*req info de la peticion request y res info de la respuesta response    */
    { res.send(usersfile); /*metodo res para sendfile*/
    console.log('users.json'); /*con esto mando un mensaje a la consola*/
});

//GET cuentas
app.get(uri + 'cuentas',
    function(req, res) /*req info de la peticion request y res info de la respuesta response (funciòn callback)   */
    { res.send(cuentasfile); /*metodo res para sendfile*/
    console.log('cuentas.json');} /*con esto mando un mensaje a la consola*/
);

//GET user con ID especifico
app.get(uri + 'users/:id',
    function(req, res)/*req info de la peticion request y res info de la respuesta response    */
    { let idUser = req.params.id; //let es ìdem al var y carga en idUser la respuesta del valor id
      res.send(usersfile[idUser - 1]); /*metodo res para obtener el usuario solicitado en la url, ejemplo ocalhost:3000/api-uruguay/v1/users/2*/
});

app.listen(port); /*metodo que crea el objeto express y es el puerto por el que va a escuchar*/
console.log('Escuchando puerto 3000 nuevo');

//GET cuentas con Qry buscando por usuario y apellido
//esto es una prueba por que ya tenemos un metodo GET anterior, por eso a la uri le puse user? como texto en lugar de users
//
app.get(uri + "user?",
    function(req, res){ /*req info de la peticion request y res info de la respuesta response (funciòn callback)   */
      let msgresponse = {'msg':'Usuario No encontrado'};
      let encontrado = false;
      console.log('GET con qry string');  /*con esto mando un mensaje a la consola*/
      usersfile.forEach(function(user) //tomamos el array userfile e iteramos asigandole a use el valor de cada registro
          {
          if(user.first_name == req.query.qfirstname && user.last_name == req.query.qlast_name) {
              msgresponse = {'msg':'Usuario encontrado',user};
              encontrado = true;
              return;
           }
      })
      var statuscode = encontrado ? 200 : 404;
      res.send(msgresponse); /*metodo res para sendfile*/
});

//POST users por body
app.post(uri + 'users',
    function(req, res){ /*req info de la peticion request y res info de la respuesta response    */
      console.log('POST users'); /*con esto mando un mensaje a la consola*/
      console.log(req.body);
      let tam = usersfile.length + 1; //para obtener el tamaño del array de usuarios
      var userNew = { //creo el usuario con las variables del body en este caso
        'id': tam,
        'first_name': req.body.first_name,
        'last_name' : req.body.last_name,
        'email' : req.body.email,
        'password' : req.body.password
      };
      usersfile.push(userNew); //metes en el array el nuevo usuario
      res.send({'ms':'Nuevo usuario registrado correctamente',userNew}); /*metodo res para sendfile*/
});

//POST users por  header
app.post(uri + 'users2',
    function(req, res){ /*req info de la peticion request y res info de la respuesta response    */
      console.log('POST users2'); /*con esto mando un mensaje a la consola*/
      console.log(req.headers);
      let tam = usersfile.length + 1; //para obtener el tamaño del array de usuarios
      var userNew = { //creo el usuario con las variables del headers en este caso
        'id': tam,
        'first_name': req.headers.first_name,
        'last_name' : req.headers.last_name,
        'email' : req.headers.email,
        'password' : req.headers.password
      };
      usersfile.push(userNew); //metes en el array el nuevo usuario
      res.send({'ms':'Nuevo usuario registrado correctamente',userNew}); /*metodo res para sendfile*/
});

//PUT user con ID especifico para modificar
app.put(uri + 'users/:id',
    function(req, res){/*req info de la peticion request y res info de la respuesta response    */
      console.log('PUT users');
      let idUpdate = req.params.id;
      let existe = true;
      if (usersfile[idUpdate - 1] == undefined) {
        console.log('Usuario NO existe');
        existe = false; }
      else {
        let userNew = { //creo el usuario con las variables del body en este caso
          'id': idUpdate,
          'first_name': req.body.first_name,
          'last_name' : req.body.last_name,
          'email' : req.body.email,
          'password' : req.body.password
        }
        usersfile[idUpdate - 1] = userNew;
      }
      var msgresponse = existe ? {'msg':'Usuario Modificado'} : {'msg':'No se pudo modificar el usuario'};
      var statuscode = existe ? 200 : 400
      res.status(statuscode)
      res.send(msgresponse);
});

//Delete user con ID especifico para eliminar
app.delete(uri + 'users/:id',
    function(req, res){/*req info de la peticion request y res info de la respuesta response    */
      console.log('Delete users');
      let idUpdate = req.params.id;
      let existe = true;
      if (usersfile[idUpdate - 1] == undefined) {
        console.log('Usuario NO existe');
        existe = false; }
      else {
        usersfile.splice(idUpdate-1,1); //borra un elemento del array
      }
      var msgresponse = existe ? {'msg':'Usuario Eliminado'} : {'msg':'No se pudo eliminar el usuario'};
      var statuscode = existe ? 200 : 404 //còdigo OK 200 y error 404
      res.status(statuscode)
      res.send(msgresponse);
});

//var urimLab = "https://api.mlab.com/api/1/databases/echubduruguay/collections/user?apiKey=c0QVmDDCQezJaftNB3r50sBGuyrchlax"
